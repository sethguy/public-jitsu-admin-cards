import * as firebase from 'firebase'

// See firebase setup in above google firebase documentation url
export const config =  {
  apiKey: "AIzaSyBqWQvKtWjFXJjOGOEc-iPUUNc1f7irbus",
  authDomain: "eppic-fire.firebaseapp.com",
  databaseURL: "https://eppic-fire.firebaseio.com",
  projectId: "eppic-fire",
  storageBucket: "eppic-fire.appspot.com",
  messagingSenderId: "699458564241"
};

firebase.initializeApp(config);
const firestore = firebase.firestore();
const settings = { /* your settings... */
  timestampsInSnapshots: true
};
firestore.settings(settings);
export default firebase;
