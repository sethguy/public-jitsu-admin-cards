import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EpiccModuleBuilderComponent } from './Components/epicc-module-builder/epicc-module-builder.component';

const components = [EpiccModuleBuilderComponent]

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ ...components],
  exports:[...components]
})
export class EpiccModuleBuilderModule { }
