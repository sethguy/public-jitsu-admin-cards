import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpiccModuleBuilderComponent } from './epicc-module-builder.component';

describe('EpiccModuleBuilderComponent', () => {
  let component: EpiccModuleBuilderComponent;
  let fixture: ComponentFixture<EpiccModuleBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpiccModuleBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpiccModuleBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
