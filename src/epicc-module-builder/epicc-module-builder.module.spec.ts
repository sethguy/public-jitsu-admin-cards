import { EpiccModuleBuilderModule } from './epicc-module-builder.module';

describe('EpiccModuleBuilderModule', () => {
  let epiccModuleBuilderModule: EpiccModuleBuilderModule;

  beforeEach(() => {
    epiccModuleBuilderModule = new EpiccModuleBuilderModule();
  });

  it('should create an instance', () => {
    expect(epiccModuleBuilderModule).toBeTruthy();
  });
});
