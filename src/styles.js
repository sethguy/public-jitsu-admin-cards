import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // You can add global styles to this file, and also import other style files
  'import': '"~@angular/material/prebuilt-themes/indigo-pink.css"'
});
