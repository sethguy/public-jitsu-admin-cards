import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import firebase from '../../firebase'

@Component({selector: 'app-edit-screen', templateUrl: './edit-screen.component.html', styleUrls: ['./edit-screen.component.css']})
export class EditScreenComponent implements OnInit {
  itemId;
  firebase = firebase
  collectionId;
  itemFields = []
  title = "";
  widgetProps;

  constructor(private route : ActivatedRoute) {}

  ngOnInit() {

    var {url, params, queryParams} = this.route.snapshot;
    var [path] = url;

    var {collectionId, itemId} = params;
    console.log('collectionId',collectionId)
    if (itemId) {
      this.widgetProps = {
        itemId,
        collectionId,
        firebase
      }
    }
    var {itemFields} = queryParams;

    this.getTitle({url, params, queryParams})

    if (collectionId && itemId) {

      this.collectionId = collectionId;
      this.itemId = itemId;

    } else if (collectionId && itemFields) {

      this.collectionId = collectionId;
      const prepItemData : any = {};

      this.itemFields = itemFields
        .split(',')
        .map(field => {
          var [fieldName,
            fieldValue] = field.split(':');
          prepItemData[fieldName] = fieldValue;
          return {
            fieldName,
            fieldValue: fieldValue || ""
          }
        })
      if (prepItemData.parent && !prepItemData.questionTemplate && (path.toString() === 'add')) {
        this.getDefaultTemplate(prepItemData.parent)

      }
    } //if item feilds
  }

  getDefaultTemplate = async(parentId) => {

    const result = await firebase
      .firestore()
      .collection('EpiccModules')
      .doc(parentId)
      .get();
    if (result.exists) {
      const data = result.data();
      if (data.defaultTemplate) {
        var {url, params, queryParams} = this.route.snapshot;
        var [path] = url;
        var {collectionId, itemId} = params;
        this.widgetProps = {
          collectionId,
          firebase,
          initTemplate: data.defaultTemplate
        }
        this
          .itemFields
          .push({fieldName: 'questionTemplate', fieldValue: data.defaultTemplate})
      }
    }

  }

  onAddNewFeild(event) {

    var {itemUpdate} = event;

    var {url, params, queryParams} = this.route.snapshot;

    var {collectionId, itemId} = params;

    var [path] = url;

    if (path.toString() == "add") {

      window.location.hash = 'edit/' + collectionId + "/" + itemUpdate.id

    } else if (path.toString() == 'edit') {

      window
        .location
        .reload()
    }
  }

  onFieldUpdate(event) {
    var {itemData} = event;
  }

  onEditEvent(event) {
    if (event.update) {
      Object
        .keys(event.update)
        .forEach((updateKey) => {
          const updateValue = event.update[updateKey]
          this.itemFields = this
            .itemFields
            .map(item => {
              if (item.fieldName == updateKey) {
                return {fieldName: updateKey, fieldValue: updateValue}
              }
              return {
                ...item
              }
            }) //item feild loop
        }) //update keys loop
    }
  }

  onClickHome() {
    window.location.hash = 'dashboard'
  }

  onPressSave(event) {
    var {itemData, itemUpdate} = event;

    var {url, params, queryParams} = this.route.snapshot;

    var {collectionId, itemId} = params;

    var [path] = url;

    if (path.toString() == "add") {

      window.location.hash = 'edit/' + collectionId + "/" + itemUpdate.id;

    } else if (path.toString() == 'edit') {

      window
        .location
        .reload()
    }

  }

  onFileData(event) {

    if (event.uploadComplete) {
      var {url, params, queryParams} = this.route.snapshot;

      var {collectionId, itemId} = params;

      var [path] = url;

      if (path.toString() == "add") {

        window.location.hash = 'edit/' + collectionId + "/" + itemId;

      } else if (path.toString() == 'edit') {

        window
          .location
          .reload()
      }
    }

  }

  getTitle(props) {

    var {url, params, queryParams} = props

    var {collectionId, itemId} = params;

    var [path] = url;

    if (path == "add") {

      this.title = "Add " + collectionId

    } else {

      this.title = "Edit "

    }

  }

}
