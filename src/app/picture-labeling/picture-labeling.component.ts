import {Component, OnInit, Input} from '@angular/core';

import * as interact from 'interactjs'

// this is used later in the resizing and gesture demos
@Component({selector: 'picture-labeling', templateUrl: './picture-labeling.component.html', styleUrls: ['./picture-labeling.component.css']})
export class PictureLabelingComponent implements OnInit {
  @Input()props;
  region : any = {
    rect: {
      width: 200,
      height: 200,
      x: 0,
      y: 0
    }
  }
  itemData : any;
  savedRegion;

  constructor() {}

  ngOnInit() {

    var {itemId, collectionId, firebase} = this.props;

    if (itemId && firebase) {

      firebase
        .firestore()
        .collection(collectionId)
        .doc(itemId)
        .get()
        .then(documentResponse => {

          this.itemData = {
            id: documentResponse.id,
            ...documentResponse.data()
          }

          var {pictureLabeling} = this.itemData;
          if(pictureLabeling && pictureLabeling.selectionRegion ){

            this.savedRegion = pictureLabeling.selectionRegion

          }

        })

    }

    this.initInteractCongfig();
  }

  saveOptions() {

    var {itemId, collectionId, firebase} = this.props;

    if (itemId && firebase) {

      firebase
        .firestore()
        .collection(collectionId)
        .doc(itemId)
        .update({pictureLabeling:{
          selectionRegion:this.region
        }})
        .then(documentResponse => {
          window.location.reload()
        })
    }

  }
  setRegion({rect, x, y, parent}) {

    this.region = {
      rect,
      x,
      y,
      parent,
      perh: ((rect.height / parent.height) * 100) + "%",
      perw: ((rect.width / parent.width) * 100) + "%",
      pery: ((y / parent.height) * 100) + "%",
      perx: ((x / parent.width) * 100) + "%"
    }

  }

  initInteractCongfig() {
    var resizableConfig = {
      // resize from all edges and corners
      edges: {
        left: true,
        right: true,
        bottom: true,
        top: true
      },

      // keep the edges inside the parent
      restrictEdges: {
        outer: 'parent',
        endOnly: true
      },

      // minimum size
      restrictSize: {
        min: {
          width: 50,
          height: 50
        }
      },

      inertia: true
    }

    interact('.resize-drag')
      .draggable({
      onmove: this.dragMoveListener,
      restrict: {
        restriction: 'parent',
        elementRect: {
          top: 0,
          left: 0,
          bottom: 1,
          right: 1
        }
      }
    })
      .resizable(resizableConfig)
      .on('resizemove', (event : any) => {
        var target = event.target,
          x = (parseFloat(target.getAttribute('data-x')) || 0),
          y = (parseFloat(target.getAttribute('data-y')) || 0);

        // update the element's style
        target.style.width = event.rect.width + 'px';
        target.style.height = event.rect.height + 'px';

        // translate when resizing from top or left edges
        x += event.deltaRect.left;
        y += event.deltaRect.top;

        var {height, width} = target
          .parentNode
          .getBoundingClientRect()

        this.setRegion({
          rect: event.rect,
          x,
          y,
          parent: {
            height,
            width
          }
        })

        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px)';

        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
      });

  }

  dragMoveListener = (event) => {

    var target = event.target,
      // keep the dragged position in the data-x/data-y attributes
      x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
      y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
    // translate the element
    target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    var {height, width} = target
      .parentNode
      .getBoundingClientRect()

    this.setRegion({
      ...this.region,
      x,
      y,
      parent: {
        height,
        width
      }
    })
  }

}
