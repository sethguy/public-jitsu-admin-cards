import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PictureLabelingComponent } from './picture-labeling.component';

describe('PictureLabelingComponent', () => {
  let component: PictureLabelingComponent;
  let fixture: ComponentFixture<PictureLabelingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PictureLabelingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PictureLabelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
