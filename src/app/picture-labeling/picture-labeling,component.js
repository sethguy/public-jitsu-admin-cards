import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'resize-drag': {
    'backgroundColor': 'transparent',
    'color': 'white',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'fontFamily': 'sans-serif',
    'width': [{ 'unit': 'px', 'value': 200 }],
    'height': [{ 'unit': 'px', 'value': 200 }],
    'border': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'blue' }],
    // This makes things *much* easier
    'boxSizing': 'border-box',
    'zIndex': '10',
    'position': 'absolute'
  }
});
