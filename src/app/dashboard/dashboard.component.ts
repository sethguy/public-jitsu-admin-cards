import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import firebase from '../../firebase'

import {LanguageAndCommunicationAreasOfConcentraionParams, CognitionAreasOfConcentraionParams} from './demoDropConfig'
@Component({selector: 'app-dashboard', templateUrl: './dashboard.component.html', styleUrls: ['./dashboard.component.css']})
export class DashboardComponent implements OnInit {
  CognitionAreasOfConcentraionParams = CognitionAreasOfConcentraionParams;
  LanguageAndCommunicationAreasOfConcentraionParams = LanguageAndCommunicationAreasOfConcentraionParams;
  buttonLabelKey = 'name'

  buttonFLexProps : any = [
    {
      collectionId: "pathways",
      buttonLabelKey: 'name',
      title: "Pathways"
    }
  ]

  firebase = firebase;
  searchTerm;
  collectionId;
  customActions;
  titlePathString = ""

  constructor(private route : ActivatedRoute) {}

  ngOnInit() {

    this
      .route
      .queryParams
      .subscribe((params) => {
        this.updateProps(params)
      })

    this.customActions = [
      {
        type: "Edit",
        label: 'Edit'
      }
    ]

  }

  goToAddItem() {

    var {collectionId, searchTerm} = this.buttonFLexProps[0]

    var path = '/add/' + collectionId

    var params = ""

    if (searchTerm && collectionId == 'questions') {

      params = `itemFields=parent:${searchTerm},questionText,answer`

    } else {

      params = `itemFields=name,parentId:${searchTerm}`

    }

    window.location.hash = path + "?" + params

  }

  onTabChange(event) {

    if (event.index == 0) {

      window.location.hash = '/dashboard?collectionId=pathways&key=bLsumLRI8yOVXaAepaOT&title=Cognition'

    } else {

      window.location.hash = '/dashboard?collectionId=pathways&key=gKdcQeizVNRqV83PhDes&title=Language And Com' +
          'munication'

    }

  }

  onButtonItemEvent(event) {

    var {collectionId} = event.getProps();

    var oldTitle = this.buttonFLexProps[0].title;
    var titleUpdate = oldTitle + " > " + event.name
    if ((oldTitle == event.name) || (oldTitle == "Pathways")) {

      titleUpdate = event.name

    }

    window.location.hash = '/dashboard?collectionId=' + collectionId + "&key=" + event.id + "&title=" + event.name //titleUpdate

  }

  onItemEvent(event) {

    var {collectionId} = event.getProps();

    var oldTitle = this.buttonFLexProps[0].title;
    var titleUpdate = oldTitle + " > " + event.name
    if ((oldTitle == event.name) || (oldTitle == "Pathways")) {

      titleUpdate = event.name

    }

    window.location.hash = 'dashboard?collectionId=' + collectionId + "&key=" + event.id + "&title=" + event.name //titleUpdate

  }

  onCardAction(cardActionEven) {

    var {action, cardData} = cardActionEven
    var {url, params, queryParams} = this.route.snapshot;
    var {collectionId} = this.buttonFLexProps[0]
    var {itemData} = cardData;

    switch (action.type) {
      case 'Edit':
        window.location.hash = 'edit/' + collectionId + "/" + itemData.id
        break;

      default:
        break;
    }

  }

  onBack(){
    window.history.back()
  }

  updateProps(params) {

    var {collectionId, key, title} = params

    switch (collectionId) {
      case 'pathways':
        {
          var nextButtonProps = {
            collectionId: "Areas of Concentration",
            title,
            whereKey: "parentId",
            searchTerm: key,
            buttonLabelKey: 'name'
          }

          this.buttonFLexProps = [
            {
              ...nextButtonProps
            }
          ]
          return;
        }
      case 'Areas of Concentration':
        {

          var nextButtonProps = {
            collectionId: "EpiccModules",
            title,
            whereKey: "parentId",
            searchTerm: key,
            buttonLabelKey: 'name'
          }

          this.buttonFLexProps = [
            {
              ...nextButtonProps
            }
          ]
          return;
        }
      case 'EpiccModules':
        {
          var nextButtonProps = {
            collectionId: "questions",
            title,
            whereKey: "parent",
            searchTerm: key,
            buttonLabelKey: 'questionText'
          }

          this.buttonFLexProps = [
            {
              ...nextButtonProps
            }
          ]
          return;
        }

      case 'EpiccSubModules':
        {
          var nextButtonProps = {
            collectionId: "questions",
            title,
            whereKey: "parentId",
            searchTerm: key,
            buttonLabelKey: 'name'
          }

          this.buttonFLexProps = [
            {
              ...nextButtonProps
            }
          ]
          return;
        }
      default:
        {
          /*var nextButtonProps = {
          collectionId: "pathways",
          title: "Pathways",
          whereKey: null,
          searchTerm: null,
          buttonLabelKey: 'name'
        }

        this.buttonFLexProps = [
          {
            ...nextButtonProps
          }
        ]*/
          return;
        }

    }

  }

  onLogout() {

    firebase
      .auth()
      .signOut()
      .catch((error) => {});

  }

}
