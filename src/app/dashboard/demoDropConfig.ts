const EpiccSubModulesKeyParams = {
  dropLabel: "name",
  collectionId: "EpiccSubModules",
  whereKey: "parentId",
  fontStyle: {
    color: "green",
    left: 30
  }

}

const EpiccModulesKeyParams = {
  dropLabel: "name",
  collectionId: "EpiccModules",
  whereKey: "parentId",
  subSearchTermKey: "id",
  subListConfig: {
    ...EpiccSubModulesKeyParams
  },
  fontStyle: {
    color: "grey",
    left: 20
  }
}

export const list = [
  {
    label: "1",
    sublist: [
      {
        label: "4"
      }, {
        label: "7"
      }
    ]
  }, {
    label: "3"
  }, {
    label: "2"
  }
]

export const CognitionAreasOfConcentraionParams = {
  dropLabel: "name",
  collectionId: "Areas of Concentration",
  whereKey: "parentId",
  searchTerm: "bLsumLRI8yOVXaAepaOT",
  subSearchTermKey: "id",

  fontStyle: {
    color: "blue",
    left: 10
  },
  subListConfig: {
    ...EpiccModulesKeyParams
  }

}

export const LanguageAndCommunicationAreasOfConcentraionParams = {
  dropLabel: "name",
  collectionId: "Areas of Concentration",
  whereKey: "parentId",
  searchTerm: "gKdcQeizVNRqV83PhDes",
  subSearchTermKey: "id",

  fontStyle: {
    color: "blue",
    left: 10
  },
  subListConfig: {
    ...EpiccModulesKeyParams
  }

}
