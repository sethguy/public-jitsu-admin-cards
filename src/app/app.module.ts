import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { EditScreenComponent } from './edit-screen/edit-screen.component'
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';

import { app_routing } from './app.routing';

import { JitsuAdminComponentsModule } from 'jitsu-admin-components';

import { EpiccModuleBuilderModule } from '../epicc-module-builder/epicc-module-builder.module';

import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import { MultipleChoiceOptionsComponent } from './multiple-choice-options/multiple-choice-options.component';
import { PictureLabelingComponent } from './picture-labeling/picture-labeling.component';
import { QuestionEditFormComponent } from './question-edit-form/question-edit-form.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EditScreenComponent,
    LoginComponent,
    MultipleChoiceOptionsComponent,
    PictureLabelingComponent,
    QuestionEditFormComponent,
  ],
  imports: [
    MatCardModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    app_routing,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    JitsuAdminComponentsModule,
    EpiccModuleBuilderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
