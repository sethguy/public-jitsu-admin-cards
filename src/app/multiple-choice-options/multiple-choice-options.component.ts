import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import firebase from '../../firebase'

import {v4} from 'uuid';
@Component({selector: 'multiple-choice-options', templateUrl: './multiple-choice-options.component.html', styleUrls: ['./multiple-choice-options.component.css']})
export class MultipleChoiceOptionsComponent implements OnInit {
  @Input()props;
  itemData
  multipleChoiceOptions : Array < any > = []
  constructor(private route : ActivatedRoute) {}

  ngOnInit() {

    var {itemId, collectionId, firebase} = this.props;

    if (itemId && firebase) {

      firebase
        .firestore()
        .collection(collectionId)
        .doc(itemId)
        .get()
        .then(documentResponse => {

          this.itemData = {
            id: documentResponse.id,
            ...documentResponse.data()
          }

          var {multipleChoiceOptions} = this.itemData;
          if(multipleChoiceOptions ){

            this.multipleChoiceOptions = multipleChoiceOptions

          }

        })

    }

  }

  saveOptions() {

    var {itemId} = this.props;

    console.log('save options', this.multipleChoiceOptions);
    var {url, params, queryParams} = this.route.snapshot;
    var [path] = url;
    var {collectionId} = params;

      firebase
        .firestore()
        .collection(collectionId)
        .doc(itemId)
        .update({multipleChoiceOptions: this.multipleChoiceOptions})
        .then(udateResponse => {

          window
            .location
            .reload()

        })

  }

  addNewOption() {

    this
      .multipleChoiceOptions
      .push({id: v4()})
  }

  removeOptions(selectedOption) {

    this.multipleChoiceOptions = [
      ...this
        .multipleChoiceOptions
        .filter((option) => {

          return option.id != selectedOption.id

        })
    ]
  }


}
