import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

import {DashboardComponent} from './dashboard/dashboard.component'
import { EditScreenComponent } from './edit-screen/edit-screen.component'

const app_routes: Routes = [

  { path: '', pathMatch: 'full', component: DashboardComponent },
  { path: 'login', pathMatch: 'full', component: LoginComponent },

  { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
  { path: 'collections/:collectionId', pathMatch: 'full', component: DashboardComponent },
  { path: 'edit/:collectionId/:itemId', pathMatch: 'full', component: EditScreenComponent },
  { path: 'add/:collectionId', pathMatch: 'full', component: EditScreenComponent },

];

export const app_routing = RouterModule.forRoot(app_routes, { useHash: true });
