import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'question-edit-form', templateUrl: './question-edit-form.component.html', styleUrls: ['./question-edit-form.component.css']})
export class QuestionEditFormComponent implements OnInit {
  @Input('props')props : any;
  @Output()onEditEvent = new EventEmitter();
  questionTemplate = 'Basic';
  constructor() {}
  ngOnInit() {
    var {itemId, collectionId, firebase} = this.props;
    if (itemId && firebase) {
      firebase
        .firestore()
        .collection(collectionId)
        .doc(itemId)
        .get()
        .then(documentResponse => {
          var {questionTemplate} = documentResponse.data();
          if (questionTemplate) {
            this.questionTemplate = questionTemplate
          }
        })
    } else {
      this.questionTemplate = this.props.initTemplate || 'Basic';
    }
  }
  selectQuestionTemplate(name) {

    var {itemId, collectionId, firebase} = this.props;

    if (itemId && firebase) {
      firebase
        .firestore()
        .collection(collectionId)
        .doc(itemId)
        .update({questionTemplate: name})
        .then(documentResponse => {
          window
            .location
            .reload()
          this.questionTemplate = name;
        })
    } else {
      this.questionTemplate = name;
      this
        .onEditEvent
        .emit({
          update: {
            questionTemplate: this.questionTemplate
          }
        })
    }
  }

}
