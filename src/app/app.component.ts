import {Component, OnInit, NgZone} from '@angular/core';

import firebase from '../firebase'
@Component({selector: 'app-root', templateUrl: './app.component.html', styleUrls: ['./app.component.css']})
export class AppComponent implements OnInit {
  user;
  loading = true
  incoming = "/dashboard"

  constructor(private zone : NgZone) {

    if (window.location.hash != '#/login') {
      this.incoming = window.location.hash;
    }

    firebase
      .auth()
      .onAuthStateChanged((user) => {
        this
          .zone
          .run(() => {
            this.loading = false;

            if (user) {

              var email = user.email;
              var uid = user.uid;
              this.user = {
                email,
                uid
              }

              window.location.hash = this.incoming

            } else {

              console.log("its not", user)
              window.location.hash = "/login"

            }

          })
      });

  }

  ngOnInit() {}
}
