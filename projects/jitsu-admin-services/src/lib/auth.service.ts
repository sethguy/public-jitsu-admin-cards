import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class AuthService {

  constructor() {}

  init(token) {
    return this
      .initTokenVerification(token)
      .then(adminProfile => {
        if (adminProfile && adminProfile.userId) {
          if (token){
            localStorage.setItem('jitsu-admin-token', token)
          }
          return adminProfile;
        } else {
          localStorage.removeItem('jitsu-admin-token')
          return {};
        }
      })
  }

  initTokenVerification(token) {

    if (token) {
      return this.verifyJwtToken(token)
    } else {
      return this
        .getCurrentUserToken()
        .then(idToken => {
          if (idToken) {
            return this.verifyJwtToken(idToken)
          } else {
            return {}
          }
        })
    }

  }

  getCurrentUserToken() {
    var token = localStorage.getItem('jitsu-admin-token')

    return Promise.resolve(token)

  }
  logout(){

    localStorage.removeItem('jitsu-admin-token')

  }

  verifyJwtToken = (token) => {

    var getCollectionUrl = "https://us-central1-jitsuadmin.cloudfunctions.net/verifyJitsuAdminToken";

    return fetch(getCollectionUrl, {
      body: JSON.stringify({token}),
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      }
    }).then(data => data.json())

  }

}
