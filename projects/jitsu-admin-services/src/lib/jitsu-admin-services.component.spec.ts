import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuAdminServicesComponent } from './jitsu-admin-services.component';

describe('JitsuAdminServicesComponent', () => {
  let component: JitsuAdminServicesComponent;
  let fixture: ComponentFixture<JitsuAdminServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuAdminServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuAdminServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
