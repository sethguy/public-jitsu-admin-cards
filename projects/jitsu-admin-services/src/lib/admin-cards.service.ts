import {Injectable} from '@angular/core';
import {getItemFields} from './utils'
import { AuthService } from './auth.service';

@Injectable({providedIn: 'root'})
export class AdminCardsService {

  constructor(private auth:AuthService) {}

  getCards(params) {


    return  this.auth.getCurrentUserToken()

      .then(idToken => {

        if (params.collectionId) {

          return this.getCollection(params.collectionId, idToken,params.applicationId)

        }

        return this.getCollections(idToken,params.applicationId)

      })

  }

  getCollection(collectionId, idToken,applicationId) {

    var getCollectionUrl = "https://us-central1-jitsuadmin.cloudfunctions.net/getCollection";

    return fetch(getCollectionUrl, {
      body: JSON.stringify({collectionId}),
        method: "POST",
        headers: {
          "jitsu-admin-token": idToken,
          "Content-Type": "application/json"
        }
      })
      .then(data => data.json())
      .then((result) => {

        var listTransform = result.map((data) => {

          var customActions = data['_jitsu_admin_actions_'] || []
          console.log(customActions)
          return {

            ...data,
            itemFields: getItemFields(data).itemFields,
            actions: [

              ...customActions,
              {
                label: "Edit",
                type: "Edit"
              }, {
                label: "Delete",
                type: "Edit"
              }
            ]
          }
        })

        return listTransform
      })
  }

  getCollections(idToken,applicationId) {

    var getCollectionsUrl = "https://us-central1-jitsuadmin.cloudfunctions.net/getCollections";

    return fetch(getCollectionsUrl, {
        headers: {
          "jitsu-admin-token": idToken

        }
      })
      .then(data => data.json())
      .then((collectionNames) => {

        var listTransform = collectionNames.map((collectionName) => {
          return {
            title: collectionName,
            actions: [

              {
                label: "GoTo",
                type: "GoTo"
              }, {
                label: "Delete",
                type: "Delete"
              }
            ]
          }
        })

        return listTransform

      })
  }

}
