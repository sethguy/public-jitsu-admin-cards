import { NgModule } from '@angular/core';
import {jitsuAdminServices} from './jitsuAdminServices'

@NgModule({
  imports: [
  ],
  declarations: [],
  exports: [],
  providers:[jitsuAdminServices]
})
export class JitsuAdminServicesModule { }
