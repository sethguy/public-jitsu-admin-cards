import { TestBed, inject } from '@angular/core/testing';

import { AdminCardsService } from './admin-cards.service';

describe('AdminCardsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminCardsService]
    });
  });

  it('should be created', inject([AdminCardsService], (service: AdminCardsService) => {
    expect(service).toBeTruthy();
  }));
});
