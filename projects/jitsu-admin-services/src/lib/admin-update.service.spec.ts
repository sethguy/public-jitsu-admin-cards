import { TestBed, inject } from '@angular/core/testing';

import { AdminUpdateService } from './admin-update.service';

describe('AdminUpdateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminUpdateService]
    });
  });

  it('should be created', inject([AdminUpdateService], (service: AdminUpdateService) => {
    expect(service).toBeTruthy();
  }));
});
