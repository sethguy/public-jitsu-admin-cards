const getItemFields = (itemData) => {

  var itemTransform = {
    itemData,
    itemFields: Object
      .keys(itemData)
      .filter((fieldName) => fieldName != "_jitsu_admin_actions_")
      .filter((fieldName) => fieldName != "id")
      .map(fieldName => {

        return {fieldName, fieldValue: itemData[fieldName]}

      })

  }

  return itemTransform

}

export {getItemFields}
