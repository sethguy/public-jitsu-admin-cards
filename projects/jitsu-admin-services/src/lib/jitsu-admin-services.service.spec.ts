import { TestBed, inject } from '@angular/core/testing';

import { JitsuAdminServicesService } from './jitsu-admin-services.service';

describe('JitsuAdminServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JitsuAdminServicesService]
    });
  });

  it('should be created', inject([JitsuAdminServicesService], (service: JitsuAdminServicesService) => {
    expect(service).toBeTruthy();
  }));
});
