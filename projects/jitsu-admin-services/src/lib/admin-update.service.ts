import {Injectable} from '@angular/core';

import {getItemFields} from './utils'
import {AuthService} from './auth.service';

@Injectable({providedIn: 'root'})
export class AdminUpdateService {

  constructor(private auth : AuthService) {}

  getItemData(params) {

    var {collectionId, itemId} = params

    return this
      .auth
      .getCurrentUserToken()
      .then(token => {

        return this
          .getItemRequest(collectionId, itemId, token)
          .then(itemData => getItemFields(itemData))

      })

  }

  getItemRequest(collectionId, itemId, idToken) {

    var getCollectionUrl = "https://us-central1-jitsuadmin.cloudfunctions.net/getItem";

    return fetch(getCollectionUrl, {
      body: JSON.stringify({collectionId, itemId}),
      method: "POST",
      headers: {
        "jitsu-admin-token": idToken,
        "Content-Type": "application/json"
      }
    }).then(data => data.json())

  }

  saveItemData(collectionId, itemData) {

    return this
      .auth
      .getCurrentUserToken()
      .then(token => {

        return this
          .saveItemRequest(collectionId, itemData, token)
          .then(itemData => getItemFields(itemData))

      })

  }

  saveItemRequest(collectionId, itemData, idToken) {

    var getCollectionUrl = "https://us-central1-jitsuadmin.cloudfunctions.net/saveItem";

    return fetch(getCollectionUrl, {
      body: JSON.stringify({collectionId, itemData}),
      method: "POST",
      headers: {
        "jitsu-admin-token": idToken,
        "Content-Type": "application/json"
      }
    }).then(data => data.json())

  }

}
