import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {AuthService} from 'jitsu-admin-services'
@Component({selector: 'app-topbar', templateUrl: './topbar.component.html', styleUrls: ['./topbar.component.css']})
export class TopbarComponent implements OnInit {
  @Input('title')title;
  @Output('onLogout')onLogout = new EventEmitter()
  @Output('onClickHome')onClickHome = new EventEmitter()

  constructor() {}

  ngOnInit() {}

  logout() {
    this
      .onLogout
      .emit()
  }
  home() {
    this
      .onClickHome
      .emit()

  }
  back() {
    window
      .history
      .back()
  }

}
