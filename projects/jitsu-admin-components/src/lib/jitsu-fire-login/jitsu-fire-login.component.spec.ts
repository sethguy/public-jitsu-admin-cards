import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuFireLoginComponent } from './jitsu-fire-login.component';

describe('JitsuFireLoginComponent', () => {
  let component: JitsuFireLoginComponent;
  let fixture: ComponentFixture<JitsuFireLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuFireLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuFireLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
