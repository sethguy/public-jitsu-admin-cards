import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'jitsu-fire-login', templateUrl: './jitsu-fire-login.component.html', styleUrls: ['./jitsu-fire-login.component.css']})
export class JitsuFireLoginComponent implements OnInit {
  @Input('firebase')firebase;
  @Output('onLoginEvent')onLoginEvent = new EventEmitter();
  email = "";
  password = ""
  constructor() {}

  ngOnInit() {}

  emailSignIn = () => this
    .firebase
    .auth()
    .signInWithEmailAndPassword(this.email, this.password)
    .then((fireSigninResponse) => this.onLoginEvent.emit({type: "fireSigninResponse", fireSigninResponse}))
    .catch((fireSigninError) => this.onLoginEvent.emit({type: "fireSigninError", fireSigninError}));

  emailSignUp = () => this
    .firebase
    .auth()
    .createUserWithEmailAndPassword(this.email, this.password)
    .then((fireSignUpResponse) => this.onLoginEvent.emit({type: "fireSignUpResponse", fireSignUpResponse}))
    .catch((fireSignUpError) => this.onLoginEvent.emit({type: "fireSignUpError", fireSignUpError}));

}
