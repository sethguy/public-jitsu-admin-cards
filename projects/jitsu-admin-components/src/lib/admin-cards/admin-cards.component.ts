import {Component, OnInit, Input, Output,EventEmitter} from '@angular/core';

@Component({selector: 'app-admin-cards', templateUrl: './admin-cards.component.html', styleUrls: ['./admin-cards.component.css']})
export class AdminCardsComponent implements OnInit {
  @Input('cards') cards:Array<any>;
  @Output('onCardAction') onCardAction = new EventEmitter();
  @Input('customActions')customActions : Array < any >;

  constructor() {}

  ngOnInit() {

  }

  onAdminCardAction(action, cardData) {

    this.onCardAction.emit({action,cardData})

  }

}
