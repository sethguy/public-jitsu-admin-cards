import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecursiveDropListComponent } from './recursive-drop-list.component';

describe('RecursiveDropListComponent', () => {
  let component: RecursiveDropListComponent;
  let fixture: ComponentFixture<RecursiveDropListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecursiveDropListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecursiveDropListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
