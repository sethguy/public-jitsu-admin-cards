import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'recursive-drop-list',
  templateUrl: './recursive-drop-list.component.html',
  styleUrls: ['./recursive-drop-list.component.css']
})
export class RecursiveDropListComponent implements OnInit {
  @Input('list') list:Array<any>;
  @Output('onItemEvent') onListItemEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  onItemEvent(event){

    this.onListItemEvent.emit(event)

  }

}
