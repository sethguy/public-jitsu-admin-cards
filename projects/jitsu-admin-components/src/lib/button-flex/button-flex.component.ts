import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'button-flex',
  templateUrl: './button-flex.component.html',
  styleUrls: ['./button-flex.component.css']
})
export class ButtonFlexComponent implements OnInit {
  @Input('list') list:Array<any>;
  @Input('buttonLabelKey') buttonLabelKey;
  @Output('onItemEvent') onListItemEvent = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }
  onItemEvent(event){

    this.onListItemEvent.emit(event)

  }
}
