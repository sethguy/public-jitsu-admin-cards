import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuAdminImageUploadComponent } from './jitsu-admin-image-upload.component';

describe('JitsuAdminImageUploadComponent', () => {
  let component: JitsuAdminImageUploadComponent;
  let fixture: ComponentFixture<JitsuAdminImageUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuAdminImageUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuAdminImageUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
