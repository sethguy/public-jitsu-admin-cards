import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'jitsu-admin-image-upload', templateUrl: './jitsu-admin-image-upload.component.html', styleUrls: ['./jitsu-admin-image-upload.component.css']})
export class JitsuAdminImageUploadComponent implements OnInit {
  @Output('onFileData')onFileData = new EventEmitter();
  @Output('onFilePreview')onFilePreview = new EventEmitter();

  imagePreview = "";
  previewFile;
  previewCanvas;
  constructor() {}

  ngOnInit() {}

  onImageInputUpdate(event) {
    this
      .getCanvasFromInputFile(event.target.files[0])
      .then((canvas : any) => {
        this.previewFile = event.target.files[0]
        this.previewCanvas = canvas;
        this.imagePreview = canvas.toDataURL()
        this
          .previewCanvas
          .toBlob((fileBlob) => this.onFilePreview.emit({canvas, previewFile: this.previewFile, fileBlob}))
      })
  }
  cancelUploaded() {
    this.previewFile = null;
    this.imagePreview = "";
  }
  uploadImage() {
    if (this.previewCanvas) {
      this
        .previewCanvas
        .toBlob((fileBlob) => this.onFileData.emit({canvas: this.previewCanvas, previewFile: this.previewFile, fileBlob}))
    }
  }
  getCanvasFromInputFile(file) {
    return new Promise((resolve, reject) => {
      var canvas = document.createElement('canvas');
      var canvas2dContext = canvas.getContext('2d');
      var reader = new FileReader();
      reader.onload = (event) => {
        var img = new Image();
        img.onload = () => {
          canvas.width = img.width;
          canvas.height = img.height;
          canvas2dContext.drawImage(img, 0, 0);
          resolve(canvas)
        }
        img.src = event.target['result'];
      }
      reader.readAsDataURL(file);
    });
  }
}
