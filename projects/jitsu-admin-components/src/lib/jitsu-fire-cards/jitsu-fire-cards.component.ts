import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'jitsu-fire-cards', templateUrl: './jitsu-fire-cards.component.html', styleUrls: ['./jitsu-fire-cards.component.css']})
export class JitsuFireCardsComponent implements OnInit {
  @Input('props')props;
  @Input('firebase')firebase;
  @Input('customActions')customActions : Array < any >;
  @Output('onCardAction')onCardActionEvent = new EventEmitter();
  cards = [];
  constructor() {}

  ngOnInit() {

    var {collectionId, whereKey, searhTerm} = this.props;

    if (collectionId) {

      this
        .selectQuery()
        .then(({docs}) => docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        })).map((data) => this.getItemFields(data)))
        .then(cards => {

          this.cards = cards;

        })
    }
  }

  onCardAction({action, cardData}) {

    this
      .onCardActionEvent
      .emit({action, cardData})

  }

  getItemFields = (itemData) => {

    var itemTransform = {
      itemData,
      itemFields: Object
        .keys(itemData)
        .filter((fieldName) => fieldName != "id")
        .map(fieldName => {

          return {fieldName, fieldValue: itemData[fieldName]}

        })

    }

    return itemTransform;

  }

  selectQuery() {

    var {collectionId, whereKey, searchTerm} = this.props;

    if (collectionId && whereKey) {

      return this
        .firebase
        .firestore()
        .collection(collectionId)
        .where(whereKey, '==', searchTerm)
        .get()

    } else if (collectionId) {

      return this
        .firebase
        .firestore()
        .collection(collectionId)
        .get()

    }

  }

}
