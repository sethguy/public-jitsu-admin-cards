import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuFireCardsComponent } from './jitsu-fire-cards.component';

describe('JitsuFireCardsComponent', () => {
  let component: JitsuFireCardsComponent;
  let fixture: ComponentFixture<JitsuFireCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuFireCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuFireCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
