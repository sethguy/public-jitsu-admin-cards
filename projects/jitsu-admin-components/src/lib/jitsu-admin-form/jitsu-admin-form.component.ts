import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
@Component({selector: 'jitsu-admin-form', templateUrl: 'jitsu-admin-form.component.html', styleUrls: ['jitsu-admin-form.component.css']})
export class JitsuAdminFormComponent implements OnInit {

  @Output('onFieldUpdate')fieldUpdateEvent = new EventEmitter();
  @Output('onAddNewFeild')onAddNewFeild = new EventEmitter();
  @Output('onPressSave')onPressSave = new EventEmitter();

  @Input()itemData = {};
  @Input()itemFields = [];

  newFieldName = "";
  newFieldInput;
  showNewinputField = false;
  constructor() {}

  ngOnInit() {}

  save() {

    this
      .onPressSave
      .emit({
        itemData: {
          ...this.itemData
        }
      })

  }

  onFieldUpdate({fieldName, value}) {

    this.itemData[fieldName] = value;

    this
      .fieldUpdateEvent
      .emit({
        itemData: {
          ...this.itemData
        }
      })

  }

  addField() {

    this.newFieldInput = "";
    this.showNewinputField = true;

  }

  cancelNewField() {

    this.showNewinputField = false;
    this.newFieldInput = "";

  }

  addNewField() {

    var updatedItemData = {
      ...this.itemData,
      [this.newFieldInput]: ""
    }
    this
      .onAddNewFeild
      .emit({updatedItemData})

  }

}
