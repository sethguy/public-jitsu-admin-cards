import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuAdminFormComponent } from './jitsu-admin-form.component';

describe('JitsuAdminFormComponent', () => {
  let component: JitsuAdminFormComponent;
  let fixture: ComponentFixture<JitsuAdminFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuAdminFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuAdminFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
