import { Component, OnInit,Output,Input,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-admin-update-input',
  templateUrl: './admin-update-input.component.html',
  styleUrls: ['./admin-update-input.component.css']
})
export class AdminUpdateInputComponent implements OnInit {
  @Input('field') field
  @Output() onChange = new EventEmitter()
  @Output('onBlur') onBlur
  placeholder=""
  inputText=""
  constructor() { }

  ngOnInit() {
    this.placeholder = this.field.fieldName
    this.inputText = this.field.fieldValue

  }


  onInputChange = (event) =>{


    this.onChange.emit({
      event,
      fieldName:this.field.fieldName,
      value:this.inputText
    })


  }

}
