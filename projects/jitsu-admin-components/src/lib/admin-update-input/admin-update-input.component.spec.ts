import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUpdateInputComponent } from './admin-update-input.component';

describe('AdminUpdateInputComponent', () => {
  let component: AdminUpdateInputComponent;
  let fixture: ComponentFixture<AdminUpdateInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUpdateInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUpdateInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
