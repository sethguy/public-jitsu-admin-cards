import { NgModule } from '@angular/core';
import {jitsuAdminComponents} from './jitsuAdminComponents'
import {materialModules} from './material-modules';
import { FormsModule }   from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [
        FormsModule,
    ...materialModules,
    BrowserAnimationsModule
  ],
  declarations: [...jitsuAdminComponents],
  exports: [...jitsuAdminComponents]
})
export class JitsuAdminComponentsModule { }
