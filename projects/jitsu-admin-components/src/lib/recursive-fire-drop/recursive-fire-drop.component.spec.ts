import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecursiveFireDropComponent } from './recursive-fire-drop.component';

describe('RecursiveFireDropComponent', () => {
  let component: RecursiveFireDropComponent;
  let fixture: ComponentFixture<RecursiveFireDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecursiveFireDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecursiveFireDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
