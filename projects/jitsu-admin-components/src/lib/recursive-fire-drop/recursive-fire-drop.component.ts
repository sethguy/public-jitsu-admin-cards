import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'recursive-fire-drop', templateUrl: './recursive-fire-drop.component.html', styleUrls: ['./recursive-fire-drop.component.css']})
export class RecursiveFireDropComponent implements OnInit {
  @Output('onItemEvent')onListItemEvent = new EventEmitter();

  @Input('props')props : any = {}
  @Input('firebase')firebase;
  list = [];
  constructor() {}

  ngOnInit() {
    var {collectionId, whereKey, searchTerm, subSearchTermKey, subListConfig} = this.props;
    if (collectionId) {
      this
        .selectQuery()
        .then(response => this.setList(response))
    }
  }

  setList(response) {
    var {subSearchTermKey, subListConfig, collectionId} = this.props;

    var {docs} = response;

    this.list = docs.map(doc => {

      return {
        id: doc.id,
        ...doc.data()
      }

    }).map(data => {
      return {
        ...data,
        subListConfig: {
          ...subListConfig,
          searchTerm: data[subSearchTermKey]
        },
        getProps: () => ({
          ...this.props
        })
      }
    })
  }

  onItemEvent(event) {

    this
      .onListItemEvent
      .emit(event)

  }

  selectQuery() {

    var {collectionId, whereKey, searchTerm} = this.props;

    if (collectionId && whereKey) {

      return this
        .firebase
        .firestore()
        .collection(collectionId)
        .where(whereKey, '==', searchTerm)
        .get()

    } else if (collectionId) {

      return this
        .firebase
        .firestore()
        .collection(collectionId)
        .get()

    }

  }

}
