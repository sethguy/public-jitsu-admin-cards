import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'cursor-pointer': {
    'cursor': 'pointer'
  }
});
