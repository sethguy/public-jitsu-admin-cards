import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'jitsu-fire-upload', templateUrl: './jitsu-fire-upload.component.html', styleUrls: ['./jitsu-fire-upload.component.css']})
export class JitsuFireUploadComponent implements OnInit {
  @Output('onFileData')onFileDataEvent = new EventEmitter();
  @Output('onFilePreview')onFilePreviewEvent = new EventEmitter();
  @Input()firebase : any;
  @Input()collectionId;
  @Input()itemId;
  constructor() {}

  ngOnInit() {

  }

  onFilePreview(event) {
    var {} = event
    this.onFilePreviewEvent.emit(event)
  }

  onFileData(event) {

    var {} = event
    this.onFileDataEvent.emit(event)
    this.uploadImage(event)
  }

  setItemImage = (imageUrl)=>{

  var {onFileData} = this

    if (this.firebase && this.collectionId && this.itemId) {

      this
        .firebase
        .firestore()
        .collection(this.collectionId)
        .doc(this.itemId)
        .update({
          imageUrl
        })

        .then( ()=>{

          this.onFileDataEvent.emit({uploadComplete:true})

        } )

    }

  }

  uploadImage(onFileDataEvent) {
    var {previewFile} = onFileDataEvent;
    var {collectionId, itemId,setItemImage,firebase} = this

    var storageRef = firebase
      .storage()
      .ref();

    var uploadTask = storageRef
      .child(`images_${collectionId}_${itemId}.jpg`)
      .put(previewFile);

    uploadTask.on('state_changed', function (snapshot : any) {

      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
      }
    }, function (error) {}, function () {

      uploadTask
        .snapshot
        .ref
        .getDownloadURL()
        .then(function (downloadURL) {

          setItemImage(downloadURL)
        })

    });

  }

}
