import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuFireUploadComponent } from './jitsu-fire-upload.component';

describe('JitsuFireUploadComponent', () => {
  let component: JitsuFireUploadComponent;
  let fixture: ComponentFixture<JitsuFireUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuFireUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuFireUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
