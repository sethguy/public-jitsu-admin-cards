import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitsuFormFireComponent } from './jitsu-form-fire.component';

describe('JitsuFormFireComponent', () => {
  let component: JitsuFormFireComponent;
  let fixture: ComponentFixture<JitsuFormFireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsuFormFireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitsuFormFireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
