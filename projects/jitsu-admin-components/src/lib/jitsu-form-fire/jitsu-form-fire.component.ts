import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'jitsu-form-fire', templateUrl: './jitsu-form-fire.component.html', styleUrls: ['./jitsu-form-fire.component.css']})
export class JitsuFormFireComponent implements OnInit {
  @Output('onFieldUpdate')fieldUpdateEvent = new EventEmitter();
  @Output('onAddNewFeild')onAddNewFeildEvent = new EventEmitter();
  @Output('onPressSave')onPressSaveEvent = new EventEmitter();
  @Input()firebase : any;
  @Input()collectionId;
  @Input()itemId;
  @Input()itemFields;

  itemData;

  constructor() {}

  ngOnInit() {

    this.initItemData()
  }

  initItemData() {

    if (this.firebase && this.collectionId && this.itemId) {

      this
        .firebase
        .firestore()
        .collection(this.collectionId)
        .doc(this.itemId)
        .get()
        .then(doc => {

          return {
            id: doc.id,
            ...doc.data()
          }

        })
        .then((itemData) => {

          this.itemData = itemData;

          this.itemFields = [
            ...this.itemFields,
            ...this
              .getItemFields(itemData)
              .itemFields
          ]

        })

    } else if (this.itemFields && this.firebase && this.collectionId) {

      this.itemData = this
        .itemFields
        .reduce((itemData, field) => {

          var {fieldName, fieldValue} = field;
          return {
            ...itemData,
            [fieldName]: fieldValue
          }

        }, {})

    }

  }

  onAddNewFeild(event) {
    var {updatedItemData} = event;
    this.itemData = updatedItemData
    this
      .saveItem()
      .then(itemUpdate => {
        this
          .onAddNewFeildEvent
          .emit({event, itemUpdate})
      })
  }

  onFieldUpdate(event) {
    var {itemData} = event;
    this.itemData = itemData
    this
      .fieldUpdateEvent
      .emit(event)
  }

  onPressSave(event) {
    var {itemData} = event;
    this.itemData = itemData
    this
      .saveItem()
      .then(itemUpdate => {
        this
          .onPressSaveEvent
          .emit({event, itemUpdate})
      })
  }

  getItemFields = (itemData) => {
    var itemTransform = {
      itemData,
      itemFields: Object
        .keys(itemData)
        .filter((fieldName) => fieldName != "_jitsu_admin_actions_")
        .filter((fieldName) => fieldName != "id")
        .map(fieldName => {
          return {fieldName, fieldValue: itemData[fieldName]}
        })

        .filter(({fieldName,fieldValue}) => !Array.isArray(fieldValue) )

        .filter(({fieldName,fieldValue}) => (typeof fieldValue =='string') || (typeof fieldValue =='number') || (typeof fieldValue =='boolean') )

    }
    return itemTransform;
  }

  saveItem() {

    if (this.firebase && this.collectionId && this.itemId) {

      return this
        .firebase
        .firestore()
        .collection(this.collectionId)
        .doc(this.itemId)
        .update({
          ...this.itemData
        })

    } else if (this.firebase && this.collectionId) {

      return this
        .firebase
        .firestore()
        .collection(this.collectionId)
        .add({
          ...this.itemData
        })

    }

  }

}
