import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FireButtonFlexComponent } from './fire-button-flex.component';

describe('FireButtonFlexComponent', () => {
  let component: FireButtonFlexComponent;
  let fixture: ComponentFixture<FireButtonFlexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FireButtonFlexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FireButtonFlexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
