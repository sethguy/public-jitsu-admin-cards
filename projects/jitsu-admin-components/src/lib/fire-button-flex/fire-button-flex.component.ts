import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({selector: 'fire-button-flex', templateUrl: './fire-button-flex.component.html', styleUrls: ['./fire-button-flex.component.css']})
export class FireButtonFlexComponent implements OnInit {
  @Output('onItemEvent')onListItemEvent = new EventEmitter();
  @Input('props')props : any = {}
  @Input('firebase')firebase;
  buttonLabelKey;
  list = [];
  constructor() {}

  ngOnInit() {
    var {
      collectionId,
      whereKey,
      searchTerm,
      subSearchTermKey,
      buttonLabelKey,
      subListConfig
    } = this.props;
    this.buttonLabelKey = buttonLabelKey
    if (collectionId) {
      this
        .selectQuery()
        .then(response => this.setList(response))
    }
  }
  onItemEvent(event) {

    this
      .onListItemEvent
      .emit(event)

  }
  setList(response) {
    var {subSearchTermKey, subListConfig} = this.props;

    var {docs} = response;

    this.list = docs.map(doc => {

      return {
        id: doc.id,
        ...doc.data()
      }

    }).map(data => {
      return {
        ...data,
        getProps: () => ({
          ...this.props
        })
      }
    })

  }

  selectQuery() {

    var {collectionId, whereKey, searchTerm} = this.props;

    if (collectionId && whereKey) {

      return this
        .firebase
        .firestore()
        .collection(collectionId)
        .where(whereKey, '==', searchTerm)
        .get()

    } else if (collectionId) {

      return this
        .firebase
        .firestore()
        .collection(collectionId)
        .get()

    }

  }

}
