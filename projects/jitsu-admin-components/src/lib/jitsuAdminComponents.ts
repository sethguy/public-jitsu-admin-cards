import {TopbarComponent} from './topbar/topbar.component';
import {AdminCardsComponent} from './admin-cards/admin-cards.component';
import {JitsuFormFireComponent} from './jitsu-form-fire/jitsu-form-fire.component';
import {JitsuAdminFormComponent} from './jitsu-admin-form/jitsu-admin-form.component';
import {JitsuFireLoginComponent} from './jitsu-fire-login/jitsu-fire-login.component';
import {JitsuFireCardsComponent} from './jitsu-fire-cards/jitsu-fire-cards.component';
import {JitsuFireUploadComponent} from './jitsu-fire-upload/jitsu-fire-upload.component';
import {AdminUpdateInputComponent} from './admin-update-input/admin-update-input.component';
import {JitsuAdminImageUploadComponent} from './jitsu-admin-image-upload/jitsu-admin-image-upload.component';
import { RecursiveDropListComponent } from './recursive-drop-list/recursive-drop-list.component';
import { RecursiveFireDropComponent } from './recursive-fire-drop/recursive-fire-drop.component';

import { ButtonFlexComponent } from './button-flex/button-flex.component';
import { FireButtonFlexComponent } from './fire-button-flex/fire-button-flex.component';

export const jitsuAdminComponents = [
  TopbarComponent,
  AdminCardsComponent,
  JitsuFormFireComponent,
  JitsuAdminFormComponent,
  JitsuFireLoginComponent,
  JitsuFireCardsComponent,
  JitsuFireUploadComponent,
  AdminUpdateInputComponent,
  JitsuAdminImageUploadComponent,
  RecursiveDropListComponent,
  RecursiveFireDropComponent,
  ButtonFlexComponent,
  FireButtonFlexComponent
]
